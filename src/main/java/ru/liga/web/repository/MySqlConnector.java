package ru.liga.web.repository;

import ru.liga.web.SqlConnectExeption;

import java.sql.*;

public class MySqlConnector {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/rubel_web?serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    public Connection connection() {
        try {
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            conn.setAutoCommit(false);

            return conn;
        } catch (SQLException e) {
            throw  new SqlConnectExeption("Error in ConnectionSupplier", e);
        }
    }
}