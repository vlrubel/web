package ru.liga.web.repository;

import ru.liga.*;
import ru.liga.web.SqlConnectExeption;
import ru.liga.web.model.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;

public class TransactionRepository implements ru.liga.web.repository.Repository<TransactionModel> {

    private MySqlConnector mySqlConnector;

    public TransactionRepository(MySqlConnector mySqlConnector) {
        this.mySqlConnector = mySqlConnector;
    }

    @Override
    public TransactionModel save(TransactionModel model) {
        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transactionmodel_tbl (name, amount, profit_of_loss, type_id, account_id) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.setString(2, String.valueOf(model.getAmount()));
                preparedStatement.setString(3, String.valueOf(model.getProfitOrLoss()));
                preparedStatement.setString(4, String.valueOf(model.getTransactionType().getId()));
                preparedStatement.setString(5, String.valueOf(model.getAccountUsersModel().getId()));
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();

                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new SqlConnectExeption("Error in save TransactionRepository", e);
            }

        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in save TransactionRepository", e);
        }





    }

//    private Long saveOneRow( String name, String amount, String profitOfLoss, String typeId, String account_id){
//
//    }


    @Override
    public void remove(long id) {
        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transactionmodel_tbl WHERE id = " + id);
                 PreparedStatement preparedStatement1 = connection.prepareStatement
                         ("SELECT type_id FROM transactionmodel_tbl WHERE id = " + id);
                 PreparedStatement preparedStatement2 = connection.prepareStatement
                         ("SELECT account_id FROM transactionmodel_tbl WHERE id = " + id)) {
                ResultSet resultSet1 = preparedStatement1.executeQuery();
                resultSet1.next();
                ResultSet resultSet2 = preparedStatement2.executeQuery();
                resultSet1.next();
                int type_id = resultSet1.getInt(1);
                int account_id = resultSet2.getInt(1);
                preparedStatement.executeUpdate();
                connection.commit();
                TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(mySqlConnector);
                transactionTypeRepository.remove(type_id);
                AccountUsersRepository accountUsersRepository = new AccountUsersRepository(mySqlConnector);
                accountUsersRepository.remove(account_id);



            } catch (SQLException e) {
                connection.rollback();
                throw new SqlConnectExeption("Error in remove TransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in remove TransactionRepository", e);
        }
    }

    @Override
    public TransactionModel findByID(long id) {
        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionmodel_tbl WHERE id = " + "'" + id + "'")) {

                TransactionModel transactionModel = new TransactionModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                transactionModel.setId(resultSet.getLong(1));
                transactionModel.setName(resultSet.getString(2));
                transactionModel.setAmount(resultSet.getBigDecimal(3));
                int transactionTypeId = resultSet.getInt(4);
                int accountId = resultSet.getInt(5);
                TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(mySqlConnector);
                transactionModel.setTransactionType(transactionTypeRepository.findByID(transactionTypeId));
                AccountUsersRepository accountUsersRepository = new AccountUsersRepository(mySqlConnector);
                transactionModel.setAccountUsersModel(accountUsersRepository.findByID(accountId));
                return transactionModel;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findByID TransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findByID TransactionRepository", e);
        }
    }

    public TransactionModel findByName(String name) {
        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionmodel_tbl WHERE name = " + "'" + name + "'")) {

                TransactionModel transactionModel = new TransactionModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                transactionModel.setId(resultSet.getLong(1));
                transactionModel.setName(resultSet.getString(2));
                transactionModel.setAmount(resultSet.getBigDecimal(3));
                int transactionTypeId = resultSet.getInt(4);
                int accountId = resultSet.getInt(5);
                TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(mySqlConnector);
                transactionModel.setTransactionType(transactionTypeRepository.findByID(transactionTypeId));
                AccountUsersRepository accountUsersRepository = new AccountUsersRepository(mySqlConnector);
                transactionModel.setAccountUsersModel(accountUsersRepository.findByID(accountId));
                return transactionModel;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findByID TransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findByID TransactionRepository", e);
        }
    }

    @Override
    public void update(TransactionModel model) {
        TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(mySqlConnector);
        transactionTypeRepository.update(model.getTransactionType());
        AccountUsersRepository accountUsersRepository = new AccountUsersRepository(mySqlConnector);
        accountUsersRepository.update(model.getAccountUsersModel());
        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transactionmodel_tbl SET name = " + "'" + model.getName() + "'" +
                            ", amount = " + "'" + model.getAmount() + "'" +
                            ", profit_of_loss = " + "'" + model.getProfitOrLoss() + "'" +
                            ", type_id = " + "'" + model.getTransactionType().getId() + "'" +
                            ", account_id = " + "'" + model.getAccountUsersModel().getId() + "'" +
                            "WHERE ID = " + model.getId())) {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new SqlConnectExeption("Error in update TransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in update TransactionRepository", e);
        }
    }

    @Override
    public ArrayList<TransactionModel> findAll() {
        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transactionmodel_tbl")) {

                ArrayList<TransactionModel> transactionModels = new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    TransactionModel transactionModel = new TransactionModel();
                    transactionModel.setId(resultSet.getLong(1));
                    transactionModel.setName(resultSet.getString(2));
                    transactionModel.setAmount(resultSet.getBigDecimal(3));
                    int transactionTypeId = resultSet.getInt(4);
                    int accountId = resultSet.getInt(5);
                    TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(mySqlConnector);
                    transactionModel.setTransactionType(transactionTypeRepository.findByID(transactionTypeId));
                    AccountUsersRepository accountUsersRepository = new AccountUsersRepository(mySqlConnector);
                    transactionModel.setAccountUsersModel(accountUsersRepository.findByID(accountId));
                    transactionModels.add(transactionModel);
                }
                return transactionModels;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findAll TransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findAll TransactionRepository", e);
        }
    }
}