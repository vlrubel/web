package ru.liga.web.repository;

import ru.liga.web.SqlConnectExeption;
import ru.liga.web.model.CategoryModel;


import java.sql.*;
import java.util.ArrayList;

public class CategoryRepository implements ru.liga.web.repository.Repository<CategoryModel>{

    private MySqlConnector mySqlConnector;

    public CategoryRepository(MySqlConnector mySqlConnector) {
        this.mySqlConnector = mySqlConnector;
    }

    @Override
    public CategoryModel save(CategoryModel model) {

        try (Connection connection = mySqlConnector.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO categorymodel_tbl (name, parent_id) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS)){

                preparedStatement.setString(1, model.getNameCategory());
                preparedStatement.setString(2, String.valueOf(model.getParent_id()));
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e){
                connection.rollback();
                throw new SqlConnectExeption("Error in save CategoryRepository", e);
            }

        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in save CategoryRepository", e);
        }
    }

    @Override
    public void remove(long id) {

        try (Connection connection = mySqlConnector.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM categorymodel_tbl WHERE id = " + id)){

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new SqlConnectExeption("Error in remove CategoryRepository", e);
            }
        } catch (SQLException e){
            throw new SqlConnectExeption("Error in remove CategoryRepository", e);
        }
    }


    @Override
    public CategoryModel findByID(long id) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categorymodel_tbl WHERE id = " + "'" + id + "'")) {

                CategoryModel categoryModel = new CategoryModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                categoryModel.setId(resultSet.getInt(1));
                categoryModel.setNameCategory(resultSet.getString(2));
                categoryModel.setParent_id(resultSet.getLong(3));
                return categoryModel;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findByID CategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findByID CategoryRepository", e);
        }
    }
    public CategoryModel findByName(String name) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categorymodel_tbl WHERE name = " + "'" + name + "'")) {

                CategoryModel categoryModel = new CategoryModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                categoryModel.setId(resultSet.getInt(1));
                categoryModel.setNameCategory(resultSet.getString(2));
                categoryModel.setParent_id(resultSet.getLong(3));
                return categoryModel;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findByName categoryRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findByName categoryRepository", e);
        }
    }

    @Override
    public void update(CategoryModel model) {
        try (Connection connection = mySqlConnector.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE categorymodel_tbl SET name = " + "'" + model.getNameCategory() + "'" +
                            ", parent_id = " + "'" + model.getParent_id() + "'" +
                            "WHERE ID = " + model.getId()))  {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new SqlConnectExeption("Error in update CategoryRepository", e);
            }
        }catch (SQLException e){
            throw new SqlConnectExeption("Error in update CategoryRepository", e);
        }
    }

    @Override
    public ArrayList<CategoryModel> findAll() {
        try(Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categorymodel_tbl")) {

                ArrayList<CategoryModel> categoryModels = new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    CategoryModel categoryModel = new CategoryModel();
                    categoryModel.setId(resultSet.getInt(1));
                    categoryModel.setNameCategory(resultSet.getString(2));
                    categoryModel.setParent_id(resultSet.getLong(3));
                    categoryModels.add(categoryModel);
                }
                return categoryModels;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findAll CategoryRepository", e);
            }
        }catch (SQLException e){
            throw new SqlConnectExeption("Error in findAll CategoryRepository", e);
        }
    }
}
