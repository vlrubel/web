package ru.liga.web.repository;

import java.util.Collection;

public interface Repository<T> {

    T findByID(long id);

    Collection<T> findAll();

    void remove(long id);

    T save(T model);

    void update(T model);


}
