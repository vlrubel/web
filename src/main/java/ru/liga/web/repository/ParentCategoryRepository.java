package ru.liga.web.repository;

import ru.liga.web.SqlConnectExeption;
import ru.liga.web.model.CategoryModel;
import ru.liga.web.model.ParentCategoryModel;


import java.sql.*;
import java.util.ArrayList;

public class ParentCategoryRepository implements ru.liga.web.repository.Repository<ParentCategoryModel>{

    private MySqlConnector mySqlConnector;

    public ParentCategoryRepository(MySqlConnector mySqlConnector) {
        this.mySqlConnector = mySqlConnector;
    }

    @Override
    public ParentCategoryModel save(ParentCategoryModel model) {

        try (Connection connection = mySqlConnector.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO parent_category_tbl (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS)){

                preparedStatement.setString(1, model.getName());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e){
                connection.rollback();
                throw new SqlConnectExeption("Error in save ParentCategoryRepository", e);
            }

        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in save ParentCategoryRepository", e);
        }
    }

    @Override
    public void remove(long id) {

        try (Connection connection = mySqlConnector.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM parent_category_tbl WHERE id = " + id)){

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new SqlConnectExeption("Error in remove ParentCategoryRepository", e);
            }
        } catch (SQLException e){
            throw new SqlConnectExeption("Error in remove ParentCategoryRepository", e);
        }
    }


    @Override
    public ParentCategoryModel findByID(long id) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl WHERE id = " + "'" + id + "'")) {

                ParentCategoryModel parentCategoryModel = new ParentCategoryModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                parentCategoryModel.setId(resultSet.getLong(1));
                parentCategoryModel.setName(resultSet.getString(2));
                return parentCategoryModel;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findByID ParentCategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findByID ParentCategoryRepository", e);
        }
    }
    public ParentCategoryModel findByName(String name) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl WHERE name = " + "'" + name + "'")) {

                ParentCategoryModel parentCategoryModel = new ParentCategoryModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                parentCategoryModel.setId(resultSet.getLong(1));
                parentCategoryModel.setName(resultSet.getString(2));
                return parentCategoryModel;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findByName parentCategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findByName parentCategoryRepository", e);
        }
    }

    @Override
    public void update(ParentCategoryModel model) {
        try (Connection connection = mySqlConnector.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE parent_category_tbl SET name = " + "'" + model.getName() + "'" +
                            "WHERE ID = " + model.getId()))  {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new SqlConnectExeption("Error in update ParentCategoryRepository", e);
            }
        }catch (SQLException e){
            throw new SqlConnectExeption("Error in update ParentCategoryRepository", e);
        }
    }

    @Override
    public ArrayList<ParentCategoryModel> findAll() {
        try(Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM parent_category_tbl")) {

                ArrayList<ParentCategoryModel> parentCategoryModels = new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    ParentCategoryModel parentCategoryModel = new ParentCategoryModel();
                    parentCategoryModel.setId(resultSet.getLong(1));
                    parentCategoryModel.setName(resultSet.getString(2));

                    parentCategoryModels.add(parentCategoryModel);
                }
                return parentCategoryModels;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findAll ParentCategoryRepository", e);
            }
        }catch (SQLException e){
            throw new SqlConnectExeption("Error in findAll ParentCategoryRepository", e);
        }
    }
}
