package ru.liga.web.repository;

import ru.liga.*;
import ru.liga.web.SqlConnectExeption;
import ru.liga.web.model.*;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class AccountUsersRepository implements ru.liga.web.repository.Repository<AccountUsersModel> {

    private MySqlConnector mySqlConnector;

    public AccountUsersRepository(MySqlConnector mySqlConnector) {
        this.mySqlConnector = mySqlConnector;
    }

    @Override
    public AccountUsersModel save(AccountUsersModel model) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO accountmodel_tbl (name, amount, currency_id) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS)) {

                preparedStatement.setString(1, model.getName());
                preparedStatement.setString(2, String.valueOf(model.getAmount()));
                preparedStatement.setString(3, String.valueOf(model.getCurrencyModel().getId()));
                CurrencyRepository currencyRepository = new CurrencyRepository(mySqlConnector);
                currencyRepository.save(model.getCurrencyModel());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new SqlConnectExeption("Error in save AccountUsersRepository", e);
            }

        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in save AccountUsersRepository", e);
        }
    }


    @Override
    public void remove(long id) {

        try (Connection connection = mySqlConnector.connection()) {
            try (
                    PreparedStatement preparedStatement1 = connection.prepareStatement
                            ("SELECT * FROM accountmodel_tbl WHERE id = " + id);
                    PreparedStatement preparedStatement = connection.prepareStatement
                            ("DELETE FROM accountmodel_tbl WHERE id = " + id)
            ) {
                ResultSet resultSet = preparedStatement1.executeQuery();

                resultSet.next();
                int currency_id = resultSet.getInt(4);
                preparedStatement.executeUpdate();
                connection.commit();
                CurrencyRepository currencyRepository = new CurrencyRepository(mySqlConnector);
                currencyRepository.remove(currency_id);
            } catch (SQLException e) {
                connection.rollback();
                throw new SqlConnectExeption("Error in remove AccountUsersRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in remove AccountUsersRepository", e);
        }
    }

    @Override
    public AccountUsersModel findByID(long id) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM accountmodel_tbl WHERE id = " + "'" + id + "'")) {

                AccountUsersModel accountmodel = new AccountUsersModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                accountmodel.setId(resultSet.getInt(1));
                accountmodel.setName(resultSet.getString(2));
                accountmodel.setAmount(resultSet.getBigDecimal(3));
                int currency_id = resultSet.getInt(4);
                accountmodel.setCurrencyModel(
                        new CurrencyRepository(mySqlConnector).findByID(currency_id));
                return accountmodel;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findByID AccountUsersRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findByID AccountUsersRepository", e);
        }
    }

    public AccountUsersModel findByName(String name) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM accountmodel_tbl WHERE name = " + "'" + name + "'")) {

                AccountUsersModel accountmodel = new AccountUsersModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                accountmodel.setId(resultSet.getInt(1));
                accountmodel.setName(resultSet.getString(2));
                accountmodel.setAmount(resultSet.getBigDecimal(3));
                int currency_id = resultSet.getInt(4);
                accountmodel.setCurrencyModel(
                        new CurrencyRepository(mySqlConnector).findByID(currency_id));
                return accountmodel;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findByName AccountUsersRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findByName AccountUsersRepository", e);
        }
    }

    @Override
    public void update(AccountUsersModel model) {
        try (Connection connection = mySqlConnector.connection()) {
            CurrencyRepository currencyRepository = new CurrencyRepository(mySqlConnector);
            currencyRepository.update(model.getCurrencyModel());
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE accountmodel_tbl SET name = " + "'" + model.getName() + "'" +
                            ", amount = " + "'" + model.getAmount() + "'" +
                            ", currency_id = " + "'" + model.getCurrencyModel().getId() + "'" +
                            "WHERE ID = " + model.getId())) {

                preparedStatement.executeUpdate();
                connection.commit();


            } catch (SQLException e) {
                connection.rollback();
                throw new SqlConnectExeption("Error in update AccountUsersRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in update AccountUsersRepository", e);
        }
    }

    @Override
    public ArrayList<AccountUsersModel> findAll() {
        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM accountmodel_tbl")) {

                ArrayList<AccountUsersModel> accountmodels = new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    AccountUsersModel accountmodel = new AccountUsersModel();
                    accountmodel.setId(resultSet.getInt(1));
                    accountmodel.setName(resultSet.getString(2));
                    accountmodel.setAmount(resultSet.getBigDecimal(3));
                    int currency_id = resultSet.getInt(4);
                    accountmodel.setCurrencyModel(
                            new CurrencyRepository(mySqlConnector).findByID(currency_id));
                    accountmodels.add(accountmodel);
                }
                return accountmodels;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findAll AccountUsersRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findAll AccountUsersRepository", e);
        }
    }
}
