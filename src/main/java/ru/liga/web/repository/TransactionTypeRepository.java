package ru.liga.web.repository;

import ru.liga.*;
import ru.liga.web.SqlConnectExeption;
import ru.liga.web.model.*;
import java.sql.*;
import java.util.ArrayList;

public class TransactionTypeRepository implements ru.liga.web.repository.Repository<TransactionType> {

    private MySqlConnector mySqlConnector;

    public TransactionTypeRepository(MySqlConnector mySqlConnector) {
        this.mySqlConnector = mySqlConnector;
    }

    @Override
    public TransactionType save(TransactionType model) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transaction_type_tbl (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS)) {

                preparedStatement.setString(1, model.getName());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new SqlConnectExeption("Error in save TransactionTypeRepository", e);
            }

        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in save TransactionTypeRepository", e);
        }
    }

    @Override
    public void remove(long id) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transaction_type_tbl WHERE id = " + id)) {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new SqlConnectExeption("Error in remove TransactionTypeRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in remove TransactionTypeRepository", e);
        }
    }

    @Override
    public TransactionType findByID(long id) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_type_tbl WHERE id = " + "'" + id + "'")) {

                TransactionType transactionType = new TransactionType();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                transactionType.setId(resultSet.getInt(1));
                transactionType.setName(resultSet.getString(2));
                return transactionType;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findByID TransactionTypeRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findByID TransactionTypeRepository", e);
        }
    }

    public TransactionType findByName(String name) {

        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_type_tbl WHERE name = " + "'" + name + "'")) {

                TransactionType transactionType = new TransactionType();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                transactionType.setId(resultSet.getInt(1));
                transactionType.setName(resultSet.getString(2));
                return transactionType;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findByName TransactionTypeRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findByName TransactionTypeRepository", e);
        }
    }

    @Override
    public void update(TransactionType model) {
        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transaction_type_tbl SET name = " + "'" + model.getName() + "'" +
                            "WHERE ID = " + model.getId())) {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new SqlConnectExeption("Error in update TransactionTypeRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in update TransactionTypeRepository", e);
        }
    }

    @Override
    public ArrayList<TransactionType> findAll() {
        try (Connection connection = mySqlConnector.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_type_tbl")) {

                ArrayList<TransactionType> transactionTypes = new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    TransactionType transactionType = new TransactionType();
                    transactionType.setId(resultSet.getInt(1));
                    transactionType.setName(resultSet.getString(2));
                    transactionTypes.add(transactionType);
                }
                return transactionTypes;
            } catch (SQLException e) {
                throw new SqlConnectExeption("Error in findAll TransactionTypeRepository", e);
            }
        } catch (SQLException e) {
            throw new SqlConnectExeption("Error in findAll TransactionTypeRepository", e);
        }
    }
}