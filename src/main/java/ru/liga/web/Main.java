package ru.liga.web;


import ru.liga.web.model.AccountUsersModel;
import ru.liga.web.model.CurrencyModel;
import ru.liga.web.model.ParentCategoryModel;
import ru.liga.web.model.TransactionType;
import ru.liga.web.repository.AccountUsersRepository;
import ru.liga.web.repository.ParentCategoryRepository;
import ru.liga.web.repository.MySqlConnector;
import ru.liga.web.repository.TransactionTypeRepository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        MySqlConnector mySqlConnector = new MySqlConnector();
//        ParentCategoryModel parentCategoryModel = new ParentCategoryModel("товары для дачи");
//        parentCategoryModel.setId(5L);
//        ParentCategoryRepository parentCategoryRepository = new ParentCategoryRepository(mySqlConnector);
//        System.out.println(parentCategoryRepository.findByID(1).toString());
//        parentCategoryRepository.findAll().forEach(System.out::println);
//        System.out.println("---------------------");
////        parentCategoryRepository.save(parentCategoryModel);
////        parentCategoryRepository.findAll().forEach(System.out::println);
//        System.out.println("---------------------");
//        parentCategoryRepository.remove(4L);
//        parentCategoryRepository.findAll().forEach(System.out::println);
//        AccountUsersModel accountUsersModel = new AccountUsersModel(2, "Tom", new BigDecimal(4000), new CurrencyModel(1, "рубль", "8","p"));
//        AccountUsersRepository accountUsersRepository = new AccountUsersRepository(mySqlConnector);
//        System.out.println(accountUsersRepository.findByName("Tom").toString());
//        accountUsersRepository.findAll().forEach(System.out::println);
        TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(mySqlConnector);
        System.out.println(transactionTypeRepository.findByID(2));
        System.out.println(transactionTypeRepository.findByName("Перевод"));
        transactionTypeRepository.findAll().forEach(System.out::println);
//        transactionTypeRepository.save(new TransactionType(5,"неПеревод"));
//        transactionTypeRepository.findAll().forEach(System.out::println);
        transactionTypeRepository.update(new TransactionType(5, "нПеревод"));
        transactionTypeRepository.findAll().forEach(System.out::println);
        transactionTypeRepository.remove(5);
        transactionTypeRepository.findAll().forEach(System.out::println);






    }
}
