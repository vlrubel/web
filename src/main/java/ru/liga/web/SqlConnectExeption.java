package ru.liga.web;

public class SqlConnectExeption extends RuntimeException {
    public SqlConnectExeption(String message, Throwable exception){
        super(message,exception);
    }
}
